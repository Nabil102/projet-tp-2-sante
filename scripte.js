/* eslint-disable no-use-before-define */
/* eslint-disable prefer-const */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
'use strict'

const nom = 'Philippe Fleury'
const dateNaissance = new Date() // à vous de créer une date à l’aide des méthodes que nous verrons
const sexe = 'F'
const SituationParticuliere = 'non'

// determine la duree de la nalyse par jour
const nbrJours = parseInt(prompt("Sur combien de jours souhaitez-vous faire l'analyse ?"))
// pour savoir la consommation d'alcool par jour et place les donnees dans un tableau
let compteur = 1
let nbrConsommationAlcool
const journeesAlcool = []
while (compteur <= nbrJours) {
  nbrConsommationAlcool = parseInt(prompt("Combien de verre d'alcool vous avez bu par jour" + ' ' + 'Jour' +
    ' ' + compteur))
  journeesAlcool.push(nbrConsommationAlcool)
  compteur++
}
// permet de faire la somme et calculer la moyenne
let moyenne
let sommeHebdo = 0

for (let i = 0; i < journeesAlcool.length; i++) {
  sommeHebdo = sommeHebdo + journeesAlcool[i]
}
moyenne = sommeHebdo / journeesAlcool.length
// pour avoir la date d'aujourd'hui

const dateDeAjourdhui = new Date()
document.write(dateDeAjourdhui.toLocaleString() + '<br><br>')
document.writeln(nom + '<br><br>')

// calculer l'age a partir de la date de naissance du naissance \
function getAge (date) {
  const dateDeAjourdhui = new Date()
  const dateNaissance = new Date(date)
  let age = dateDeAjourdhui.getFullYear() - dateNaissance.getFullYear()
  const m = dateDeAjourdhui.getMonth() - dateNaissance.getMonth()
  if (m < 0 || (m === 0 && dateDeAjourdhui.getDate() < dateNaissance.getDate())) {
    age--
  }
  return age
}
document.write('Âge : ' + getAge('09/22/1982') + '<br><br>')
document.writeln('Alcool :' + '<br><br>')
document.write('Moyenne par jour : ' + moyenne.toFixed(2) + '<br><br>')

// affiche la recomandation d'alcool selon le cas et la situation
let maxAlcoolSemaine
let maxAlcoolJour
if (SituationParticuliere == 'oui') {
  maxAlcoolSemaine = 0
  maxAlcoolJour = 0
} else if (sexe == 'M') {
  maxAlcoolSemaine = 15
  maxAlcoolJour = 3
} else {
  maxAlcoolSemaine = 10
  maxAlcoolJour = 2
}

const max = Math.max(...journeesAlcool)

document.write('Consommation sur une semaine : ' + sommeHebdo + ' Recommandation : ' + maxAlcoolSemaine + '<br><br>')
document.write('Maximun en une journée : ' + max + ' Recommandation : ' + maxAlcoolJour + '<br><br>')

// calcule le nombre de jour excédants et affiche la ration des journées Excédant

let nbJoursExcédant = 0
for (i = 0; i <= journeesAlcool.length; i++) {
  if (maxAlcoolJour == 3 || maxAlcoolJour == 2) {
    if (journeesAlcool[i] > maxAlcoolJour) {
      nbJoursExcédant++
    }
  }
}

const rationJoursExcédant = (nbJoursExcédant / nbrJours) * 100
document.write('Ratio de journées excédants : ' + rationJoursExcédant.toFixed(2) + '\n' + '%' + '<br><br>')

// Calcule le ratio des journées sans alcool
let nbJoursZero = 0
for (let i = 0; i <= journeesAlcool.length; i++) {
  if (journeesAlcool[i] === 0) {
    nbJoursZero++
  }
}

const ratioJourZero = (nbJoursZero / nbrJours) * 100
document.write('Ratio de journées sans alcool : ' + ratioJourZero.toFixed(2) + '\n' + '%' + '<br><br>')

// permet de determiner si les recommandation est Respectee ou non

let recommandationAlcoolRespectee = true
if (sommeHebdo > maxAlcoolSemaine || max > maxAlcoolJour) {
  recommandationAlcoolRespectee = false
  document.write('Vous ne respectez pas les recommandations.')
} else {
  document.write('Vous respectez les recommandations.')
}
